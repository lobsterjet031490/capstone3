import {Row, Col, Card} from 'react-bootstrap';
import dog from '../image/dog.jpg';
import dog2 from '../image/dog2.jpg';
import dog3 from '../image/dog3.jpg';


export default function Highlights(){
	return(
		<Row className="my-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>
				            <h2 className="text-center">High Breed</h2>
				        </Card.Title>
				        <Card.Text className="text-center">
				            <img src={dog} className="img-fluid"/>
				        </Card.Text>
				    </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>
				            <h2 className="text-center">Pure Breed</h2>
				        </Card.Title>
				        <Card.Text className="text-center">
				           <img src={dog2} className="img-fluid"/>
				        </Card.Text>
				    </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>
				            <h2 className="text-center">Askal</h2>
				        </Card.Title>
				        <Card.Text className="text-center">
				            <img src={dog3} className="img-fluid"/>
				        </Card.Text>
				    </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}