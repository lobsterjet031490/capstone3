import {Card} from 'react-bootstrap';
import Proptypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function ProductCatalog({product}){


	const {_id, name, description, price} = product;

	
	return (
		<Card>
		    <Card.Body>
		        <Card.Title>{name}</Card.Title>

		        <Card.Subtitle>Description:</Card.Subtitle>
		        <Card.Text>{description}</Card.Text>

		        <Card.Subtitle>Price:</Card.Subtitle>
		        <Card.Text>Php {price}</Card.Text>
	
		        <Link className="btn btn-primary" to={`/products/${_id}`}>View Details</Link>
		    </Card.Body>
		</Card>	
	)		
}


	ProductCatalog.propTypes = {
	product: Proptypes.shape({
		name: Proptypes.string.isRequired,
		description: Proptypes.string.isRequired,
		price: Proptypes.number.isRequired
	})
}