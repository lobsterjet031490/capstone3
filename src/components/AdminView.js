import {Table} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import EditProduct from './EditProduct.js';
import ArchiveProduct from './ArchiveProduct.js';

export default function AdminView({productsData, fetchProducts}){
	const [products, setProducts] = useState([])

	useEffect(() => {
		const products_array = productsData.map(product => {
		return(
				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>{product.isActive ? 'Available' : 'Unavailable'}</td>
					<td>
						<EditProduct product_id={product._id} fetchProducts={fetchProducts}/>
						<ArchiveProduct product_id={product._id} fetchProducts={fetchProducts} isActive={product.isActive}/>
					</td>
				</tr>
			)
		})

		setProducts(products_array)
	}, [productsData])

	return (
		<>
			<h1>Admin Dashboard</h1>

			<Table striped bordered hover responsive>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>
		</>
		)
}