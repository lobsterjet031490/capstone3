import {Button, Modal, Form} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';

export default function EditProduct({product_id, fetchProducts}){
	
	const [productId, setProductId] = useState('');

	
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	
	const [showEditModal, setShowEditModal] = useState(false);

	const openEditModal = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			
			setProductId(result._id)
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})

		
		setShowEditModal(true)
	}

	const closeEditModal = () => {
		setShowEditModal(false);
		setName('')
		setDescription('')
		setPrice('')
	}

	const editProduct = (event, productId) => {
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				Swal.fire({
					title: 'Product Updated',
					text: 'Product successfully updated.',
					icon: 'success'
				})

				fetchProducts();
				closeEditModal();
			} else {
				Swal.fire({
					title: 'Something went wrong',
					text: 'Please try again.',
					icon: 'error'
				})

				fetchProducts();
				closeEditModal();
			}
		})
	}

	return (
		<>
		<Button variant="primary" size="sm" onClick={() => openEditModal(product_id)}>Edit</Button>

			
		<Modal show={showEditModal} onHide={closeEditModal}>
			<Form onSubmit={event => editProduct(event, productId)}>
				<Modal.Header closeButton>
					<Modal.Title>Edit Product</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form.Group controlId="productName">
                       <Form.Label>Name</Form.Label>
                        <Form.Control 
                        type="text"
                        value={name}
                        onChange={event => setName(event.target.value)}                       
                        required
                        />
                         </Form.Group>
                            <Form.Group controlId="productDescription">
                                <Form.Label>Description</Form.Label>
                                <Form.Control 
                                type="text"
                                value={description}
                                onChange={event => setDescription(event.target.value)} 
                                required
                                />
                            </Form.Group>
                            <Form.Group controlId="productPrice">
                                <Form.Label>Price</Form.Label>
                                <Form.Control 
                                type="number" 
                                value={price}
                                onChange={event => setPrice(event.target.value)}
                                required
                                />
                            </Form.Group>
				</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={closeEditModal}>Close</Button>
					<Button variant='success' type='submit'>Submit</Button>
				</Modal.Footer>
			</Form>
		</Modal>
		</>
	)
}