import {Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function Banner() {

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>Dog Shop</h1>
				<Link className="btn btn-primary" to='/products/'>Shop Now</Link>

			</Col>
		</Row>
		)
}