import {Nav, Navbar, Container} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext.js';

export default function AppNavbar(){
	const { user } = useContext(UserContext);

	return (
	<Navbar className="mb-5" bg="dark" variant="dark" expand="lg">
		<Container fluid>
			<Navbar.Brand as={Link} to='/'>Jeter's Shop</Navbar.Brand>

			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse>
					<Nav className="ms-auto">

						<Nav.Link as={NavLink} to='/'>Home</Nav.Link>
						<Nav.Link as={NavLink} to='/products'>Products</Nav.Link>
						
					{ (user.id !== null) ?
	    				user.isAdmin ?
	    			<>
		    			<Nav.Link as={NavLink} to='/products/add'>Add Product</Nav.Link>
		    			<Nav.Link as={NavLink} to='/logout'>Logout</Nav.Link>
		    		</>
		    		:
		    		<>
		    			<Nav.Link as={NavLink} to='/logout'>Logout</Nav.Link>
		    		</>
    				:	
    				<>
			    		<Nav.Link as={NavLink} to='/register'>Register</Nav.Link>
			    		<Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
    				</>		
			    }		

					</Nav>
				</Navbar.Collapse>
		</Container>
  	</Navbar>
		)
}