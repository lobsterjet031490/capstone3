import {useState, useEffect} from 'react';
import ProductCatalog from './ProductCatalog.js';

export default function UserView({productsData}){
	const [products, setProducts] = useState([])

	useEffect(() => {
		
		const active_products = productsData.map(product => {
			if(product.isActive === true){
				return (
					<ProductCatalog product={product} key={product._id}/>
					)
			} else {
				return null
			}
		})
		
		setProducts(active_products);
	}, [productsData])

	return (
		<>
			<h1>Products</h1>
			{ products }
		</>
		)
}