import './App.css';
import {Container} from 'react-bootstrap';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Products from './pages/Products.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';
import AddProduct from './pages/AddProduct.js';
import ProductItem from './pages/ProductItem.js';


import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes} from 'react-router-dom';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext.js'

function App() {

  const [user, setUser] = useState ({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: localStorage.getItem('userId')
      })
    })
    .then(response => response.json())
    .then(result => {
      if(typeof result._id !== 'undefined'){
        
        setUser({
          id: result._id,
          isAdmin: result.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
            <Container>
              <Routes>
                <Route path='/' element={ <Home/> }/>
                <Route path='/products' element={ <Products/> }/>
                <Route path='/products/add' element={ <AddProduct/> }/>
                <Route path='/products/:productId' element={ <ProductItem/> }/>
                <Route path='/register' element={ <Register/> }/>
                <Route path='/login' element={ <Login/> }/>

                <Route path='/logout' element={ <Logout/> }/>
                <Route path='*' element={ <NotFound/> }/>
                
              </Routes>
            </Container>
         </Router>
      </UserProvider>
    </>
  );
}

export default App;
