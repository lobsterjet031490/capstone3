import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext.js';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	function registerUser(event){
		
		event.preventDefault();

		
		fetch(`${process.env.REACT_APP_API_URL}/api/users/register`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password
			})
		}).then(response => response.json()).then(result =>{
			if(result){
				
				setFirstName("")
				setLastName("")
				setEmail("")
				setPassword("")
				setConfirmPassword("")

				Swal.fire({
					title: 'Register Successfully',
					text: result.message,
					icon: 'success'
				})
				navigate('/login');
			} else{

				Swal.fire({
					title: 'Registration Failed',
					text: 'Please try Again',
					icon: 'error'
				})
			}
		})
	}

	
	useEffect(() => {
		
		if((firstName !== "" && lastName !== "" && email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	}, [firstName, lastName, email, password, confirmPassword]);

return(
	(user.id !== null) ?
		<Navigate to='/products'/>
	:
		<Form onSubmit={(event) => registerUser(event)}>
		    <h1 className="my-5 text-center">Register</h1>
		        <Form.Group>
		            <Form.Label>First Name:</Form.Label>
		            <Form.Control 
		            type="text" 
		            placeholder="Enter First Name" 
		            required
		            value={firstName}
		            onChange={event => {setFirstName(event.target.value)}}
		            />
		        </Form.Group>
		        <Form.Group>
		            <Form.Label>Last Name:</Form.Label>
		            <Form.Control 
		            type="text" 
		            placeholder="Enter Last Name" 
		            required
		            value={lastName}
		            onChange={event => {setLastName(event.target.value)}}
		            />
		        </Form.Group>
		        <Form.Group>
		            <Form.Label>Email:</Form.Label>
		            <Form.Control 
		            type="email" 
		            placeholder="Enter Email" 
		            required
		            value={email}
		            onChange={event => {setEmail(event.target.value)}}
		            />
		        </Form.Group>
		        <Form.Group>
		            <Form.Label>Password:</Form.Label>
		            <Form.Control 
		            type="password" 
		            placeholder="Enter Password" 
		            required
		            value={password}
		            onChange={event => {setPassword(event.target.value)}}
		            />
		        </Form.Group>
		        <Form.Group>
		            <Form.Label>Confirm Password:</Form.Label>
		            <Form.Control 
		            type="password" 
		            placeholder="Confirm Password" 
		            required
		            value={confirmPassword}
		            onChange={event => {setConfirmPassword(event.target.value)}}
		            />
		        </Form.Group>

		        <Button variant="primary" type="submit" disabled={isActive === false}>Submit</Button>   
	    </Form>
	)
}