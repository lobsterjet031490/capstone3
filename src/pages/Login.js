import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login(){
    
    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    


    function loginUser(event){
        
        event.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(result => {
            console.log(result);

        if (result.accessToken) {
            localStorage.setItem('token', result.accessToken);
            localStorage.setItem('userId', result.userId);

            retrieveUserDetails(result.accessToken, result.userId)

          setEmail('');
          setPassword('');
          
          Swal.fire({
            title: 'Login Success',
            text: 'You have logged in successfully',
            icon: 'success'
          })

        } else {

            Swal.fire({
                title: 'Something went wrong',
                text: `${email} does not exist`,
                icon: 'warning'
            })
        }
      })
    };

    const retrieveUserDetails = (token, userId) => {
        fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: userId
            })
        })
        .then(response => response.json())
        .then(result => {
           
            setUser({
                id: result._id,
                isAdmin: result.isAdmin
            })
        })
    }

    useEffect(() => {
        setIsActive(email && password);
   },  [email, password]);

    return(
        (user.id !== null) ?
            <Navigate to='/products'/>
        :
            <Form onSubmit={(event) => loginUser(event)}>
                <h1  className="mt-3">Login</h1>
                    <Form.Group className="mt-3">
                        <Form.Label>Email address:</Form.Label>
                        <Form.Control 
                        type="email" 
                        placeholder="Enter Email address" 
                        required
                        value={email}
                        onChange={event => {setEmail(event.target.value)}}
                        />
                    </Form.Group>
                    <Form.Group className="mt-3">
                        <Form.Label>Password:</Form.Label>
                        <Form.Control 
                        type="password" 
                        placeholder="Enter Password" 
                        required
                        value={password}
                        onChange={event => {setPassword(event.target.value)}}
                        />
                    </Form.Group>
                    <Button className="mt-3" variant="primary" type="submit" disabled={!isActive}>Login</Button>
            </Form>
    )
}