import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function ProductItem(){
	const {productId} = useParams();
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [quantity, setQuantity] = useState(1)

	const [product, setProduct] = useState()

	const updateQuantity = (qty) => {
		const sum = quantity + qty
		if(sum > 0 && sum <= 10){
			setQuantity(sum)
		}
		
	}
	

	const createOrder = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/users/create_order`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: product.name,
				quantity: quantity
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result.message === 'Purchase successfully!'){
				Swal.fire({
					title: 'Successful Purchase',
					text: result.message,
					icon: 'success'
				})
				setQuantity(1)
				navigate('/products')
			}else{
				Swal.fire({
					title: "Something went wrong",
					text: 'Please try again',
					icon: 'error'
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			setProduct(result)
		})
	}, [productId])

	if(!product){
		return <p>Loading</p>
	}
console.log(product)
	return(
		<Container className="mt-5">
			<Row>
				<Col>
					<Card>
						<Card.Body className="text-center">
							<Card.Title><h1>{product.name}</h1></Card.Title>

							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{product.description}</Card.Text>

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{product.price * quantity}</Card.Text>

							{ (user.id !== null) ?
							<>
							<div className="d-flex gap-1 justify-content-center mb-2">
								<Button size="sm" onClick={() => updateQuantity(-1)}>-</Button>
								<div>Qty: {quantity}</div>
								<Button size="sm" onClick={() => updateQuantity(1)}>+</Button>
							</div>
							<Button variant="primary" onClick={() => createOrder()}>Buy now</Button>
							</>
							:
							<Link className="btn btn-danger btn-block" to='/login'>Log In to Purchase</Link>
							}
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}